﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Integration.AmpqContracts;
using MassTransit;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerEventGateway: IGivingPromoCodeToCustomerEventGateway
    {
        private readonly IBus _bus;

        public GivingPromoCodeToCustomerEventGateway(IBus bus)
        {
            _bus = bus;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };

            await _bus.Publish(dto);
        }
    }
}
