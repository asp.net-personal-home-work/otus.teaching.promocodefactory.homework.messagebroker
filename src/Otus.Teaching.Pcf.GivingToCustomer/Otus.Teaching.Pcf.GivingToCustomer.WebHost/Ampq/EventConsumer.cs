﻿using MassTransit;
using System.Threading.Tasks;
using Integration.AmpqContracts;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Ampq
{
    public class EventConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IPromoCodesService _promoCodesService;

        public EventConsumer(IPromoCodesService promoCodesService)
        {
            _promoCodesService = promoCodesService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            PromoCode promoCode = PromoCodeMapper.MapFromDto(context.Message);
            await _promoCodesService.GivePromoCodesToCustomersWithPreferenceAsync(promoCode);
        }
    }
}