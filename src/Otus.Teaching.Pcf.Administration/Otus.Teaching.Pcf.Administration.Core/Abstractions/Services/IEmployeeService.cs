﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Services
{
    public interface IEmployeeService
    {
        public Task<IReadOnlyCollection<Employee>> GetEmployeeCollection();
        public Task<Employee> GetEmployeeById(Guid id);
        public Task<bool> UpdateAppliedPromocodesAsync(Guid id);
    }
}
