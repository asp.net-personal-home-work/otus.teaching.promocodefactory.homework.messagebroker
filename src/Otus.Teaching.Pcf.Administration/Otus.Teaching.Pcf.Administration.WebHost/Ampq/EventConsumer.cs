﻿using System;
using MassTransit;
using System.Threading.Tasks;
using Integration.AmpqContracts;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;

namespace Otus.Teaching.Pcf.Administration.WebHost.Ampq
{
    public class EventConsumer : IConsumer<GivePromoCodeToCustomerDto>
    {
        private readonly IEmployeeService _employeeService;

        public EventConsumer(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        public async Task Consume(ConsumeContext<GivePromoCodeToCustomerDto> context)
        {
            if (context.Message.PartnerManagerId.HasValue)
            {
                Guid partnerManagerId = context.Message.PartnerManagerId.Value;
                await _employeeService.UpdateAppliedPromocodesAsync(partnerManagerId);
            }
        }
    }
}
