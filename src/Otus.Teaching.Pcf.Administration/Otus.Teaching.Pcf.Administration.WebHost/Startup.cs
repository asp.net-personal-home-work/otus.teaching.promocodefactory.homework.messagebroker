using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Castle.Core.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;
using Otus.Teaching.Pcf.Administration.DataAccess.Repositories;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using MassTransit;
using GreenPipes;
using Otus.Teaching.Pcf.Administration.WebHost.Ampq;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.DataAccess.Services;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddMvcOptions(x=> 
                x.SuppressAsyncSuffixInActionNames = false);
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddDbContext<DataContext>(x =>
            {
                //x.UseSqlite("Filename=PromocodeFactoryAdministrationDb.sqlite");
                x.UseNpgsql(Configuration.GetConnectionString("PromocodeFactoryAdministrationDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });

            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory Administration API Doc";
                options.Version = "1.0";
            });

            services.AddMassTransit(q =>
            {
                q.AddConsumer<EventConsumer>();
                q.UsingRabbitMq((context, config) =>
                {
                    config.Host(Configuration["RabbitMq:Host"], virtualHost: Configuration["RabbitMq:VirtualHost"], q =>
                    {
                        q.Username(Configuration["RabbitMq:Username"]);
                        q.Password(Configuration["RabbitMq:Password"]);
                    });
                    config.ReceiveEndpoint("queue:Event.Pcf.AdministrationService.GivePromoCodeToCustomerEvent", e =>
                    {
                        e.ConfigureConsumer<EventConsumer>(context);
                        e.UseMessageRetry(r =>
                        {
                            r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                        });
                    });
                });
            });
            services.AddMassTransitHostedService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
            dbInitializer.InitializeDb();
        }

        //private void Configure(IServiceCollectionBusConfigurator q)
        //{
        //    q.UsingRabbitMq((context, config) =>
        //    {
        //        config.Host(Configuration["RabbitMq:Host"], cfg =>
        //        {
        //            cfg.Username(Configuration["RabbitMq:Username"]);
        //            cfg.Password(Configuration["RabbitMq:Password"]);
        //        });

        //        RegisterEndPoints(config);
        //    });
        //}
        //private static void RegisterEndPoints(IRabbitMqBusFactoryConfigurator configurator)
        //{
        //    configurator.ReceiveEndpoint($"masstransit_event_queue", e =>
        //    {
        //        e.Consumer<EventConsumer>();
        //        e.UseMessageRetry(r =>
        //        {
        //            //r.Ignore<ArithmeticException>();
        //            r.Incremental(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        //        });
        //    });
        //}
    }
}