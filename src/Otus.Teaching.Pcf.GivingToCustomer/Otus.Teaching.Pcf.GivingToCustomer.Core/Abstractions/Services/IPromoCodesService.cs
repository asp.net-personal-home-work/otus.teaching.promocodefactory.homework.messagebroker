﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IPromoCodesService
    {
        public Task<IEnumerable<PromoCode>> GetPromoCodeList();
        public Task<bool> GivePromoCodesToCustomersWithPreferenceAsync(PromoCode request);
    }
}
