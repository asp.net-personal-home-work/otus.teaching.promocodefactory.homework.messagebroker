﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<IReadOnlyCollection<Employee>> GetEmployeeCollection()
        {
            IEnumerable<Employee> items = await _employeeRepository.GetAllAsync();
            
            return items.ToList();
        }

        public async Task<Employee> GetEmployeeById(Guid id)
        {
           return await _employeeRepository.GetByIdAsync(id);
        }

        public async Task<bool> UpdateAppliedPromocodesAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null) return false;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);

            return true;
        }
    }
}
